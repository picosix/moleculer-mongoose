"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _lodash = require("lodash.size");

var _lodash2 = _interopRequireDefault(_lodash);

var _moleculer = require("moleculer");

var _methods = require("./methods");

var _methods2 = _interopRequireDefault(_methods);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
  // Will be overwrite on using service
  name: "",
  settings: {
    mongoose: {
      connection: {
        host: process.env.MONGO_DB_HOST,
        port: process.env.MONGO_DB_PORT,
        dbname: process.env.MONGO_DB_NAME,
        username: process.env.MONGO_DB_USERNAME,
        password: process.env.MONGO_DB_PASSWORD,
        authMechanism: process.env.MONGO_DB_AUTH_MECHANISM,
        authSource: process.env.MONGO_DB_AUTH_SOURCE
      },
      connectionOptions: { useNewUrlParser: true },
      schema: {},
      schemaOptions: {
        timestamps: true,
        useNestedStrict: true
      },
      defaultLimit: Number(process.env.MONGO_DEFAULT_LIMIT)
    }
  },
  methods: _methods2.default,
  created() {
    if (!(0, _lodash2.default)(this.settings.mongoose.schema)) {
      throw new _moleculer.Errors.ValidationError("You must defined Mongoose schema first");
    }
  },
  async started() {
    return this.mongooseConnect();
  },
  async stopped() {
    return this.mongooseDisconnect();
  }
};