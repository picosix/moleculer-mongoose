"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = async function mongooseDisonnect() {
  return this.mongooseConnection.disconnect();
};