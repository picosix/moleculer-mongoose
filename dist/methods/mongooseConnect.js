"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.generateConnectString = undefined;

var _mongoose = require("mongoose");

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const generateConnectString = exports.generateConnectString = connection => {
  let target = `${connection.host}:${connection.port}/${connection.dbname}`;

  if (!connection.username || !connection.password || !connection.authMechanism || !connection.authSource) {
    return `mongodb://${target}`;
  }

  let auth = `${connection.username}:${connection.password}`;
  let auhtOptions = `?authMechanism=${connection.authMechanism}&authSource=${connection.authSource}`;
  return `mongodb://${auth}@${target}${auhtOptions}`;
};

exports.default = async function mongooseConnect() {
  const {
    connection,
    connectionOptions,
    schema,
    schemaOptions
  } = this.settings.mongoose;

  const connectString = generateConnectString(connection);
  this.mongooseConnection = await _mongoose2.default.connect(connectString, connectionOptions);

  const schemaInstance = new _mongoose2.default.Schema(schema, schemaOptions);
  this.mongooseModel = _mongoose2.default.model(this.name, schemaInstance);

  return this.mongooseModel;
};