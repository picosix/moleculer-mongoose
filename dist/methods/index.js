"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _mongooseConnect = require("./mongooseConnect");

var _mongooseConnect2 = _interopRequireDefault(_mongooseConnect);

var _mongooseDisconnect = require("./mongooseDisconnect");

var _mongooseDisconnect2 = _interopRequireDefault(_mongooseDisconnect);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = { mongooseConnect: _mongooseConnect2.default, mongooseDisconnect: _mongooseDisconnect2.default };