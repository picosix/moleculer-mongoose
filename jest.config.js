module.exports = {
  verbose: process.env.NODE_ENV === "test",
  testMatch: ["**/?(*.)+(spec|test).js"],
  collectCoverageFrom: ["src/**/*.js"],
  setupTestFrameworkScriptFile: "<rootDir>/__tests__/setup.js",
  testURL: "http://localhost/"
};
