import { ServiceBroker } from "moleculer";
import faker from "faker";

import { serviceName, schema } from "../faker";
import moleculerMongoose from "../../src";

describe("interaction.service", () => {
  let broker;
  let service;

  beforeAll(async () => {
    broker = new ServiceBroker({ logger: false });
    service = broker.createService(moleculerMongoose, {
      name: serviceName,
      settings: { mongoose: { schema } }
    });

    await broker.start();
  });

  afterAll(async () => {
    await broker.stop();
  });

  it("should query successful by mongoose model", async () => {
    let entity = { name: faker.name.firstName() };
    await service.mongooseModel.create(entity);

    let doc = await service.mongooseModel.findOne(entity);
    expect(doc.name).toBe(entity.name);
  });
});
