import { ServiceBroker, Errors } from "moleculer";

import { serviceName, schema } from "../faker";
import moleculerMongoose from "../../src";

describe("unit.index", () => {
  describe("created", () => {
    it("should throw error because of empty schema", () => {
      const broker = new ServiceBroker({ logger: false });

      expect(() => {
        broker.createService(moleculerMongoose, {
          name: serviceName
        });
      }).toThrowError(
        new Errors.ValidationError("You must defined Mongoose schema first")
      );
    });

    it("should init successful with default options", () => {
      const broker = new ServiceBroker({ logger: false });

      let service = broker.createService(moleculerMongoose, {
        name: serviceName,
        settings: { mongoose: { schema } }
      });

      expect(service.settings.mongoose).toEqual({
        connection: {
          host: process.env.MONGO_DB_HOST,
          port: process.env.MONGO_DB_PORT,
          dbname: process.env.MONGO_DB_NAME,
          username: process.env.MONGO_DB_USERNAME,
          password: process.env.MONGO_DB_PASSWORD,
          authMechanism: process.env.MONGO_DB_AUTH_MECHANISM,
          authSource: process.env.MONGO_DB_AUTH_SOURCE
        },
        connectionOptions: { useNewUrlParser: true },
        schema,
        schemaOptions: {
          timestamps: true,
          useNestedStrict: true
        },
        defaultLimit: Number(process.env.MONGO_DEFAULT_LIMIT)
      });
    });
  });

  describe("started", () => {
    it('should call "mongooseConnect" method after started', async () => {
      const broker = new ServiceBroker({ logger: false });

      let service = broker.createService(moleculerMongoose, {
        name: serviceName,
        settings: { mongoose: { schema } }
      });

      service.mongooseConnect = jest.fn();

      await broker.start();

      expect(service.mongooseConnect).toHaveBeenCalledTimes(1);
    });
  });

  describe("stopped", () => {
    it('should call "mongooseDisconnect" method after stopped', async () => {
      const broker = new ServiceBroker({ logger: false });

      let service = broker.createService(moleculerMongoose, {
        name: serviceName,
        settings: { mongoose: { schema } }
      });

      service.mongooseConnect = jest.fn();
      service.mongooseDisconnect = jest.fn();

      await broker.start();
      expect(service.mongooseConnect).toHaveBeenCalledTimes(1);

      await broker.stop();
      expect(service.mongooseDisconnect).toHaveBeenCalledTimes(1);
    });
  });
});
