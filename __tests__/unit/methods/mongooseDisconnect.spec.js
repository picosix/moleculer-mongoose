import { ServiceBroker } from "moleculer";

import { serviceName, schema } from "../../faker";
import moleculerMongoose from "../../../src";

describe("unit.methods.mongooseDisconnect", () => {
  it("should disconnect successful", async () => {
    const broker = new ServiceBroker({ logger: false });

    let service = broker.createService(moleculerMongoose, {
      name: serviceName,
      settings: { mongoose: { schema } }
    });

    service.mongooseConnection = { disconnect: jest.fn() };

    await service.mongooseDisconnect();

    expect(service.mongooseConnection.disconnect).toHaveBeenCalledTimes(1);
  });
});
