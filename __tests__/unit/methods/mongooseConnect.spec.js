import mongoose from "mongoose";
import { ServiceBroker } from "moleculer";

import { serviceName, schema } from "../../faker";
import moleculerMongoose from "../../../src";
import { generateConnectString } from "../../../src/methods/mongooseConnect";

describe("unit.methods.mongooseConnect", () => {
  describe("generateConnectString", () => {
    let connection = {
      host: process.env.MONGO_DB_HOST,
      port: process.env.MONGO_DB_PORT,
      dbname: process.env.MONGO_DB_NAME
    };

    it("should return connect string WITHOUT authentication", () => {
      const connectString = generateConnectString(connection);

      expect(connectString).toBe(
        `mongodb://${connection.host}:${connection.port}/${connection.dbname}`
      );
    });

    it("should return connect string WITH authentication", () => {
      let connectionWithAuth = {
        ...connection,
        username: "username",
        password: "password",
        authMechanism: "SCRAM-SHA-1",
        authSource: "admin"
      };
      const connectString = generateConnectString(connectionWithAuth);

      expect(connectString).toBe(
        `mongodb://${connectionWithAuth.username}:${
          connectionWithAuth.password
        }@${connectionWithAuth.host}:${connectionWithAuth.port}/${
          connectionWithAuth.dbname
        }?authMechanism=${connectionWithAuth.authMechanism}&authSource=${
          connectionWithAuth.authSource
        }`
      );
    });
  });

  describe("mongooseConnect", () => {
    beforeAll(() => {
      mongoose.connect = jest.fn(async () => ({}));
      mongoose.Schema = jest.fn().mockImplementation(() => ({}));
      mongoose.model = jest.fn(async () => ({}));
    });

    beforeEach(() => {
      mongoose.connect.mockClear();
      mongoose.Schema.mockClear();
      mongoose.model.mockClear();
    });

    it("should connect successful", async () => {
      const broker = new ServiceBroker({ logger: false });

      let service = broker.createService(moleculerMongoose, {
        name: serviceName,
        settings: { mongoose: { schema } }
      });
      let connectString = generateConnectString(
        service.settings.mongoose.connection
      );

      let mongooseModel = await service.mongooseConnect();
      expect(mongooseModel).toEqual({});

      expect(mongoose.connect).toHaveBeenCalledTimes(1);
      expect(mongoose.connect).toHaveBeenCalledWith(
        connectString,
        service.settings.mongoose.connectionOptions
      );

      expect(mongoose.Schema).toHaveBeenCalledTimes(1);
      expect(mongoose.Schema).toHaveBeenCalledWith(
        service.settings.mongoose.schema,
        service.settings.mongoose.schemaOptions
      );

      expect(mongoose.model).toHaveBeenCalledTimes(1);
      expect(mongoose.model).toHaveBeenCalledWith(service.name, {});

      expect(service.mongooseConnection).toBeTruthy();
      expect(service.mongooseModel).toBeTruthy();
    });
  });
});
