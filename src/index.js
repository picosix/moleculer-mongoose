import size from "lodash.size";
import { Errors } from "moleculer";

import methods from "./methods";

export default {
  // Will be overwrite on using service
  name: "",
  settings: {
    mongoose: {
      connection: {
        host: process.env.MONGO_DB_HOST,
        port: process.env.MONGO_DB_PORT,
        dbname: process.env.MONGO_DB_NAME,
        username: process.env.MONGO_DB_USERNAME,
        password: process.env.MONGO_DB_PASSWORD,
        authMechanism: process.env.MONGO_DB_AUTH_MECHANISM,
        authSource: process.env.MONGO_DB_AUTH_SOURCE
      },
      connectionOptions: { useNewUrlParser: true },
      schema: {},
      schemaOptions: {
        timestamps: true,
        useNestedStrict: true
      },
      defaultLimit: Number(process.env.MONGO_DEFAULT_LIMIT)
    }
  },
  methods,
  created() {
    if (!size(this.settings.mongoose.schema)) {
      throw new Errors.ValidationError(
        "You must defined Mongoose schema first"
      );
    }
  },
  async started() {
    return this.mongooseConnect();
  },
  async stopped() {
    return this.mongooseDisconnect();
  }
};
