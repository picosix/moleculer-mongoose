import mongooseConnect from "./mongooseConnect";
import mongooseDisconnect from "./mongooseDisconnect";

export default { mongooseConnect, mongooseDisconnect };
