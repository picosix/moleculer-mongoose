import mongoose from "mongoose";

export const generateConnectString = connection => {
  let target = `${connection.host}:${connection.port}/${connection.dbname}`;

  if (
    !connection.username ||
    !connection.password ||
    !connection.authMechanism ||
    !connection.authSource
  ) {
    return `mongodb://${target}`;
  }

  let auth = `${connection.username}:${connection.password}`;
  let auhtOptions = `?authMechanism=${connection.authMechanism}&authSource=${
    connection.authSource
  }`;
  return `mongodb://${auth}@${target}${auhtOptions}`;
};

export default async function mongooseConnect() {
  const {
    connection,
    connectionOptions,
    schema,
    schemaOptions
  } = this.settings.mongoose;

  const connectString = generateConnectString(connection);
  this.mongooseConnection = await mongoose.connect(
    connectString,
    connectionOptions
  );

  const schemaInstance = new mongoose.Schema(schema, schemaOptions);
  this.mongooseModel = mongoose.model(this.name, schemaInstance);

  return this.mongooseModel;
}
